
# Description of hists folder:

The hists folder is where you will find all the histograms after having run the analysis. 

- hists.json file: collection of keys used to name all the root files containing the histograms and all the keys to name each histogram inside each file 

- all the .root files with the histograms after each cut. The name of each .root file is composed by the applied cut + the observable,  therefore the final histograms will be identified by the last applied cut + all the observables.

