import pepper
import awkward as ak
import numpy as np
import coffea
from coffea import lookup_tools
import coffea.lumi_tools
import coffea.jetmet_tools
from functools import partial
from dataclasses import dataclass
from typing import Optional, Tuple
import uproot
import logging
from pepper.scale_factors import PileupWeighter, ScaleFactors, MuonScaleFactor

@dataclass
class VariationArg:
    name: Optional[str] = None
    junc: Optional[Tuple[str, str]] = None
    jer: Optional[str] = "central"
logger = logging.getLogger(__name__)
# All processors should inherit from pepper.ProcessorBasicPhysics
class Processor(pepper.ProcessorBasicPhysics):
    # We use the ConfigTTbarLL instead of its base Config, to use some of its
    # predefined extras
    config_class = pepper.ConfigBasicPhysics

    def __init__(self, config, eventdir):
        # Initialize the class, maybe overwrite some config variables and
        # load additional files if needed
        # Can set and modify configuration here as well
        config["histogram_format"] = "root"
        # Need to call parent init to make histograms and such ready
        super().__init__(config, eventdir)

        # It is not recommended to put anything as member variable into a
        # a Processor because the Processor instance is sent as raw bytes
        # between nodes when running on HTCondor.

    def process_selection(self, selector, dsname, is_mc, filler):
        # Implement the selection steps: add cuts, define objects and/or
        # compute event weights
        
        ##################################### CROSS-SECTION SCALE FACTOR ####################################
        if is_mc:
            selector.add_cut(
                "Cross section", partial(self.crosssection_scale, dsname))        
        
        #################################### OBJECT SELECTION ###############################################
        selector.set_column("Jet", self.pick_jets_dijet)
        selector.add_cut("Number of jets", self.num_jets)
        selector.add_cut("Dijet_cut", self.dijet_cond)
        selector.set_column("Dijet", self.dijet_column)

    def pick_jets_dijet(self, data):
        "This function selects events with the following cuts: jet_id = Tight, 2 jets at least, leading jet with eta < 1.3 and Delta_phi between leading and subleading jet > 2.7 " 
        # We select jets
        jets = data["Jet"]
        # We imposed Tight iD
        has_id = jets.isTight
        return jets[has_id]
    def num_jets(self, data):
        min_num_jets = ak.num(data["Jet"]) >= self.config["min_number_jets"]
        return min_num_jets

    def dijet_cond(self, data):
        # We define leading and subleading jets        
        leading_jet = data.Jet[:,0]
        subleading_jet = data.Jet[:,1]
        # We ask for leading jet with eta < 1.3 and Delta_phi between leading and subleading jet > 2.7
        eta_cond_phi_cond = ((leading_jet.eta < self.config["max_leading_jet_eta"]) & ((leading_jet - subleading_jet).phi > self.config["min_dif_phi_leading_subleading_jet"]))
        return eta_cond_phi_cond

    def dijet_column(self, data):
        return (data.Jet[:,0] + data.Jet[:,1])
