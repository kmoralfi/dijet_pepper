# Dijet analysis
Dijet analysis for jetMET implemented through Coffea.

## Description

This is a CMS analysis based on the Pepper framework. The whole code is written in:

- Perl: functions oriented to computational performance, HTCondor and XRootD routines.

- Shell: parameters inside configuration files oriented to HTCondor

- Python: functions oriented to the main analysis / physics part.

## Getting started

### 🔨 Installation
To install this framework:

```bash
git clone https://gitlab.cern.ch/kmoralfi/dijet_pepper.git

cd dijet_pepper

source plugins/enviroment.sh

python3 -m pip install --upgrade --upgrade-strategy eager --editable .
```

### 🔧 Run the analysis
First setup your enviroment:

```bash
source plugins/enviroment.sh
```

If you want to work only with datasamples form the local DESY machine, you need to make sure in dijet/dijet_config.json

```bash
file_mode: "local"
```

If you want to run everything in HTCondor using datasamples from the local DESY machine + datasameples from XRootD:
datasamples from the local DESY machine, then you need to setup your VOMS proxy first:

```bash
voms-proxy-init --voms cms --out $X509_USER_PROXY
```

Then, you need to make sure in dijet/dijet_config.json

```bash
file_mode: "local+xrootd"
```

If you want to perform a debug run (where only 1 datasample from each type: ZZ, WZ, SingleMuon etc is analysed):

```bash
python3 -m pepper.runproc --debug dijet/dijet_processor.py dijet/dijet_config.json 
```

If you want to perform a run with a specific dataset (x. ex. only ). ⚠️  Careful! The dataset name must be the same as the one used in dijet/dijet_config.json :

```bash
python3 -m pepper.runproc dijet/dijet_processor.py dijet/dijet_config.json --dataset EWK_LLJJ_MLL-50_MJJ-120_TuneCP5_13TeV-madgraph-pythia8_dipole 
```

If you want to run everything in HTCondor, you should submit as many condor jobs as datasamples (for example, if you have 10 datasamplesm then --condor 10), to avoid cosumming too much memory and getting our jobs on hold as a result, we set a lower chunksize (--chunksize 20000) and finallly to be able to retake the jobs if they get removed of put on hold, we set up a state file (--statedata dijet_state_example.coffea)

```bash
python3 -m pepper.runproc dijet/dijet_processor.py dijet/dijet_config.json --condor 10 --chunksize 20000 --statedata dijet_state_example.coffea
```

⚠️  CAUTION 

1. IMPORTANT! To avoid killing the jobs by mistake when loggin off the cms-naf or when the terminal dies, you should use tmux:

2. IMPORTANT! In case your jobs get removed or put on hold and you have tried to remove them with  

```bash
condor_rm username 
```

And it is not removing your jobs, do:

```bash
condor_rm --forcex username
```
and alternate it with the usual condor_rm username until you get your condor_q console clean again.

3. IMPORTANT! If some of your jobs get removed (because some files are corrupted x. ex.) you can still get the histograms with the jobs which have been completed by executing:

```bash
python3 scripts/export_hists_from_state.py dijet_state_example.coffea root hists
```

## 📊 How to

### The analysis structure: general

The main processor: 
```bash
dijet/dijet_processor.py
```
calls another more basic processor:
```bash
pepper/processor_basic.py
```

Which has all the cuts functions except:

- pick_jets_dijet

- num_jets

- dijet_cond

- dijet_column

Which are defined inside dijet_processor itself

All the functions take as arguments the parameters defined in dijet/dijet_config.json.

Finally, the histograms are saved in the hists directory after aplying each cut, so in the current analysis, the final histograms are the Dijet_cut_*.root

### The analysis structure: details

Following the dijet/dijet_processor.py code:

- [ ] The <mark>cross section scale factor</mark> is applied only to MC samples through the function <mark>crosssection_scale</mark>, which is defined in pepper/processor_basic.py. This function calls through dijet/dijet_config.json a dijet/dijet_files/dijet_xsec.json and produces a json file with the cross-section scale factors under the name dijet_mc_lumi.json. This is done the first time we run the analysis by executing:

```bash
python3 scripts/compute_mc_lumifactors.py dijet/dijet_config.json dijet/dijet_files/dijet_mc_lumi.json
```

- [ ] The <mark>minimun number of jets </mark> is applied thorugh the function <mark>num_jets</mark>.
- [ ] The <mark>dijet condition</mark> is applied through the function <mark>dijet_cond</mark>.

Finally we save some information in additional data entries or columns to call them when filling histograms:

- [ ] The <mark>dijet column</mark> is set through the function <mark>dijet_column</mark>.

## ⛳ Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## 📚  References

- Python (version: 3.9):
  - [Pepper](https://gitlab.cern.ch/pepper/pepper/-/tree/master/)
  - [Awkard](https://pypi.org/project/awkward/) (version:)
  - [Numpy](https://numpy.org/) (version: )
  - [Coffea](https://coffeateam.github.io/coffea/) (version: )
  - [Matplotlib](https://matplotlib.org/) (version: )
  - [Mplhep](https://mplhep.readthedocs.io/en/latest/index.html)(version: )

    Fan of Jupyter notebooks? Here you can find [here](https://hub.gke2.mybinder.org/user/coffeateam-coffea-5v3zsm55/tree/binder) some Coffea tutorials (installation free!)
- [Uproot](https://uproot.readthedocs.io/en/latest/uproot.html) (version: )
- [JSON](https://www.json.org/json-en.html) (version: )
- [README](https://docs.readme.com/)
    
